lint:
	markdownlint . || true
	yamllint . || true
	for FILE in $(find . -iname '*.json'); do echo "${FILE}"; jsonlint $FILE --quiet --compact || true; done || true
	hadolint Dockerfile || true

fix:
	markdownlint --fix .

upgrade:
	pip-upgrade -p all --skip-virtualenv-check requirements.txt

upgrade-latest:
	pip-upgrade -p all --prerelease --skip-virtualenv-check requirements.txt
