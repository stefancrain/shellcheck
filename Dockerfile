FROM alpine:3.11
WORKDIR /data
RUN apk add --no-cache \
      tini==0.18.0-r0 \
      bash==5.0.11-r1 \
      shellcheck==0.7.0-r1&& \
    adduser -D -u 1000 shellcheck

USER shellcheck
ENV SHELLCHECK_OPTS=""

ENTRYPOINT ["/sbin/tini", "--", "shellcheck" ]
CMD ["--help"]

LABEL org.label-schema.name="shellcheck`" \
      org.label-schema.description="--" \
      org.label-schema.vendor="--" \
      org.label-schema.docker.schema-version="1.0" \
      org.label-schema.vcs-type="git"
